﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PatilendirAdminPanel.Startup))]
namespace PatilendirAdminPanel
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
