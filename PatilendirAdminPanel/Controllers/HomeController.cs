﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace PatilendirAdminPanel.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            if (HttpContext.Application["LoginStatus"] == null)
            {
                return RedirectToAction("Login", "LoginRegister");
            }
            else
            {
                if (HttpContext.Application["LoginStatus"].ToString() == "1")
                {
                    return View();
                }
                else
                    return RedirectToAction("Login", "LoginRegister");
            }
        }

        public ActionResult Change(String language)
        {
            HttpCookie cookie = new HttpCookie("Language");

            if (language != null)
            {
                if (language == "TR")
                {
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("tr-TR");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("tr-TR");
                    cookie.Value = "tr-TR";
                }
                else if (language == "EN")
                {
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
                    cookie.Value = "en-US";
                }
            }

            Response.Cookies.Add(cookie);

            return View("Index");
        }
	}
}