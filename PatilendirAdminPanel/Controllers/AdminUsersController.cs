﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PatilendirAdminPanel.Models;

namespace PatilendirAdminPanel.Controllers
{
    public class AdminUsersController : Controller
    {
        private PatilendirAdminPanelEntities db = new PatilendirAdminPanelEntities();

        // GET: AdminUsers
        public ActionResult Index()
        {
            if (HttpContext.Application["LoginStatus"] == null)
            {
                return RedirectToAction("Login", "LoginRegister");
            }
            else
            {
                if (HttpContext.Application["LoginStatus"].ToString() == "1")
                {
                    var adminUser = db.AdminUser.Include(a => a.Status);
                    return View(adminUser.ToList());
                }
                else
                    return RedirectToAction("Login", "LoginRegister");
            }
        }


        // GET: AdminUsers/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdminUser adminUser = db.AdminUser.Find(id);
            if (adminUser == null)
            {
                return HttpNotFound();
            }
            return View(adminUser);
        }

        // POST: AdminUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            AdminUser adminUser = db.AdminUser.Find(id);
            db.AdminUser.Remove(adminUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
