﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PatilendirAdminPanel.Models;

namespace PatilendirAdminPanel.Controllers
{
    public class CustomersController : Controller
    {
        private PatilendirAdminPanelEntities db = new PatilendirAdminPanelEntities();

        // GET: Customers
        public ActionResult Index()
        {
            if (HttpContext.Application["LoginStatus"] == null)
            {
                return RedirectToAction("Login", "LoginRegister");
            }
            else
            {
                if (HttpContext.Application["LoginStatus"].ToString() == "1")
                {
                    var customer = db.Customer.Include(c => c.Status);
                    return View(customer.ToList());
                }
                else
                    return RedirectToAction("Login", "LoginRegister");
            }           
        }

        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customer.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Customer customer = db.Customer.Find(id);
            db.Customer.Remove(customer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
