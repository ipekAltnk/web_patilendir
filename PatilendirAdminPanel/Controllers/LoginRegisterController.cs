﻿using PatilendirAdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PatilendirAdminPanel.Controllers
{
    public class LoginRegisterController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult LoginTry(string email, string password)
        {
            ViewBag.LoginResult = "";

            PatilendirAdminPanelEntities db = new PatilendirAdminPanelEntities();

            ObjectParameter returnCode = new ObjectParameter("returnCode", typeof(Int32));
            ObjectParameter adminUserId = new ObjectParameter("adminUserId", typeof(Int64));

            db.LoginAdminUser(email, password, adminUserId, returnCode);

            if (Convert.ToInt32(returnCode.Value) == 1)
            {
                if (HttpContext.Application["LoginStatus"] == null)
                {
                    HttpContext.Application.Add("LoginStatus", "1");
                    HttpContext.Application.Add("AdminUserId", adminUserId);
                }
                else
                {
                    HttpContext.Application["LoginStatus"] = "1";
                    HttpContext.Application["AdminUserId"] = adminUserId;
                }

                return RedirectToAction("Index", "Home");
            }
            else
            {
                if (HttpContext.Application["LoginStatus"] == null)
                {
                    HttpContext.Application.Add("LoginStatus", "0");
                }
                else
                {
                    HttpContext.Application["LoginStatus"] = "0";
                }

                TempData["LoginResult"] = PatilendirAdminPanel.Resource.LoginFailed;

                return RedirectToAction("Login", "LoginRegister");
            }
        }

        public ActionResult Cikis()
        {
            if (HttpContext.Application["LoginStatus"] == null)
            {
                HttpContext.Application.Add("LoginStatus", "0");
                HttpContext.Application["AdminUserId"] = 0;
            }
            else
            {
                HttpContext.Application["LoginStatus"] = "0";
                HttpContext.Application["AdminUserId"] = 0;
            }

            return RedirectToAction("Login", "LoginRegister");
        }
    }
}