﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PatilendirAdminPanel.Models;
using System.Data.Entity.Core.Objects;

namespace PatilendirAdminPanel.Controllers
{
    public class AnimalsController : Controller
    {
        private PatilendirAdminPanelEntities db = new PatilendirAdminPanelEntities();

        // GET: Animals
        public ActionResult Index()
        {
            if (HttpContext.Application["LoginStatus"] == null)
            {
                return RedirectToAction("Login", "LoginRegister");
            }
            else
            {
                if (HttpContext.Application["LoginStatus"].ToString() == "1")
                {
                    var animal = db.Animal.Include(a => a.AnimalStatus).Include(a => a.AnimalType).Include(a => a.Gender).Include(a => a.Status).Include(a => a.Status1);
                    return View(animal.ToList());
                }
                else
                    return RedirectToAction("Login", "LoginRegister");
            }
        }

        // GET: Animals/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Animal animal = db.Animal.Find(id);
            if (animal == null)
            {
                return HttpNotFound();
            }
            return View(animal);
        }

        // GET: Animals/Create
        public ActionResult Create()
        {
            ViewBag.AnimalStatusId = new SelectList(db.AnimalStatus, "AnimalStatusId", "Name");
            ViewBag.AnimalTypeId = new SelectList(db.AnimalType, "AnimalTypeId", "Name");
            ViewBag.GenderId = new SelectList(db.Gender, "GenderId", "Name");
            ViewBag.StatusId = new SelectList(db.Status, "StatusId", "Name");
            ViewBag.StatusId = new SelectList(db.Status, "StatusId", "Name");
            return View();
        }

        // POST: Animals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AnimalId,StatusId,AnimalStatusId,Name,Description,AnimalTypeId,GenderId,YearAsMonth,ContactName,ContactTelephone,Image,InsertionDate,InsertedUserId,UpdatedDate,UpdatedUserId")] Animal animal)
        
        {
            if (ModelState.IsValid)
            {
                string staticPath = "https://dl.dropboxusercontent.com/u/67477431/imagesPets/";

                ObjectParameter returnCode = new ObjectParameter("returnCode", typeof(Int32));

                db.AddAnimal(animal.Name,animal.AnimalStatusId, animal.Description, animal.AnimalTypeId, animal.GenderId, animal.YearAsMonth, animal.ContactName, animal.ContactTelephone, staticPath + animal.Image, Convert.ToInt64(((System.Data.Entity.Core.Objects.ObjectParameter)HttpContext.Application["AdminUserId"]).Value), returnCode);
               
                if (Convert.ToInt32(returnCode.Value) == 1)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.AnimalStatusId = new SelectList(db.AnimalStatus, "AnimalStatusId", "Name", animal.AnimalStatusId);
                    ViewBag.AnimalTypeId = new SelectList(db.AnimalType, "AnimalTypeId", "Name", animal.AnimalTypeId);
                    ViewBag.GenderId = new SelectList(db.Gender, "GenderId", "Name", animal.GenderId);
                    ViewBag.StatusId = new SelectList(db.Status, "StatusId", "Name", animal.StatusId);
                    ViewBag.StatusId = new SelectList(db.Status, "StatusId", "Name", animal.StatusId);
                    return View(animal);
                }
            }

            ViewBag.AnimalStatusId = new SelectList(db.AnimalStatus, "AnimalStatusId", "Name", animal.AnimalStatusId);
            ViewBag.AnimalTypeId = new SelectList(db.AnimalType, "AnimalTypeId", "Name", animal.AnimalTypeId);
            ViewBag.GenderId = new SelectList(db.Gender, "GenderId", "Name", animal.GenderId);
            ViewBag.StatusId = new SelectList(db.Status, "StatusId", "Name", animal.StatusId);
            ViewBag.StatusId = new SelectList(db.Status, "StatusId", "Name", animal.StatusId);
            return View(animal);

        }

        // GET: Animals/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Animal animal = db.Animal.Find(id);
            if (animal == null)
            {
                return HttpNotFound();
            }
            ViewBag.AnimalStatusId = new SelectList(db.AnimalStatus, "AnimalStatusId", "Name", animal.AnimalStatusId);
            ViewBag.AnimalTypeId = new SelectList(db.AnimalType, "AnimalTypeId", "Name", animal.AnimalTypeId);
            ViewBag.GenderId = new SelectList(db.Gender, "GenderId", "Name", animal.GenderId);
            ViewBag.StatusId = new SelectList(db.Status, "StatusId", "Name", animal.StatusId);
            ViewBag.StatusId = new SelectList(db.Status, "StatusId", "Name", animal.StatusId);
            return View(animal);
        }

        // POST: Animals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AnimalId,StatusId,AnimalStatusId,Name,Description,AnimalTypeId,GenderId,YearAsMonth,ContactName,ContactTelephone,Image,InsertionDate,InsertedUserId,UpdatedDate,UpdatedUserId")] Animal animal)
        {
            if (ModelState.IsValid)
            {
                ObjectParameter returnCode = new ObjectParameter("returnCode", typeof(Int32));

                db.UpdateAnimal(animal.AnimalId, animal.AnimalStatusId, animal.Name, animal.Description, animal.AnimalTypeId, animal.GenderId, animal.YearAsMonth, animal.ContactName, animal.ContactTelephone, animal.Image, Convert.ToInt64(((System.Data.Entity.Core.Objects.ObjectParameter)HttpContext.Application["AdminUserId"]).Value), returnCode);

                if (Convert.ToInt32(returnCode.Value) == 1)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.AnimalStatusId = new SelectList(db.AnimalStatus, "AnimalStatusId", "Name", animal.AnimalStatusId);
                    ViewBag.AnimalTypeId = new SelectList(db.AnimalType, "AnimalTypeId", "Name", animal.AnimalTypeId);
                    ViewBag.GenderId = new SelectList(db.Gender, "GenderId", "Name", animal.GenderId);
                    ViewBag.StatusId = new SelectList(db.Status, "StatusId", "Name", animal.StatusId);
                    ViewBag.StatusId = new SelectList(db.Status, "StatusId", "Name", animal.StatusId);
                    return View(animal);
                }
            }

            ViewBag.AnimalStatusId = new SelectList(db.AnimalStatus, "AnimalStatusId", "Name", animal.AnimalStatusId);
            ViewBag.AnimalTypeId = new SelectList(db.AnimalType, "AnimalTypeId", "Name", animal.AnimalTypeId);
            ViewBag.GenderId = new SelectList(db.Gender, "GenderId", "Name", animal.GenderId);
            ViewBag.StatusId = new SelectList(db.Status, "StatusId", "Name", animal.StatusId);
            ViewBag.StatusId = new SelectList(db.Status, "StatusId", "Name", animal.StatusId);
            return View(animal);
        }

        // GET: Animals/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Animal animal = db.Animal.Find(id);
            if (animal == null)
            {
                return HttpNotFound();
            }
            return View(animal);
        }

        // POST: Animals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Animal animal = db.Animal.Find(id);
            db.Animal.Remove(animal);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
