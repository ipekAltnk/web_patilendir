﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Patilendir.Controllers
{
    public class Hay_HakController : Controller
    {
        public ActionResult IndexHay_Hak()
        {
            if (HttpContext.Application["LoginStatus"] == null)
            {
                return RedirectToAction("Login", "LoginRegister");
            }
            else
            {
                if (HttpContext.Application["LoginStatus"].ToString() == "1")
                    return View();
                else
                    return RedirectToAction("Login", "LoginRegister");
            }
        }
    }
}