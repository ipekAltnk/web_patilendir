﻿using Patilendir.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Patilendir.Controllers
{
    public class LoginRegisterController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult LoginTry(string email, string password)
        {
            ViewBag.LoginResult = "";

            PatilendirEntities db = new PatilendirEntities();

            ObjectParameter returnCode = new ObjectParameter("returnCode", typeof(Int32));

            db.LoginCustomer(email, password, returnCode);

            if (Convert.ToInt32(returnCode.Value) == 1)
            {
                if (HttpContext.Application["LoginStatus"] == null)
                {
                    HttpContext.Application.Add("LoginStatus", "1");
                }
                else
                {
                    HttpContext.Application["LoginStatus"] = "1";
                }

                return RedirectToAction("Index", "Home");
            }
            else
            {
                if (HttpContext.Application["LoginStatus"] == null)
                {
                    HttpContext.Application.Add("LoginStatus", "0");
                }
                else
                {
                    HttpContext.Application["LoginStatus"] = "0";
                }

                TempData["LoginResult"] = Patilendir.Resource.LoginFailed;

                return RedirectToAction("Login", "LoginRegister");
            }
        }
        public ActionResult RegisterTry(string email, string password, string name, string surname)
        {
            ViewBag.LoginResult = "";

            PatilendirEntities db = new PatilendirEntities();

            ObjectParameter returnCode = new ObjectParameter("returnCode", typeof(Int32));

            db.RegisterCustomer(email, password, name, surname, returnCode);

            if (Convert.ToInt32(returnCode.Value) == 1)
            {
                if (HttpContext.Application["LoginStatus"] == null)
                {
                    HttpContext.Application.Add("LoginStatus", "1");
                }
                else
                {
                    HttpContext.Application["LoginStatus"] = "1";
                }

                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["LoginResult"] = Patilendir.Resource.EmailInUse;

                if (HttpContext.Application["LoginStatus"] == null)
                {
                    HttpContext.Application.Add("LoginStatus", "0");
                }
                else
                {
                    HttpContext.Application["LoginStatus"] = "0";
                }

                return RedirectToAction("Login", "LoginRegister");
            }
        }
        public ActionResult Cikis()
        {
            if (HttpContext.Application["LoginStatus"] == null)
            {
                HttpContext.Application.Add("LoginStatus", "0");
            }
            else
            {
                HttpContext.Application["LoginStatus"] = "0";
            }

            return RedirectToAction("Login", "LoginRegister");
        }
    }
}