﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Patilendir.Models;

namespace Patilendir
{
    public class AnimalsController : Controller
    {
        private PatilendirEntities db = new PatilendirEntities();

        // GET: Animals
        public ActionResult UzgunIndex()
        {
            if (HttpContext.Application["LoginStatus"] == null)
            {
                return RedirectToAction("Login", "LoginRegister");
            }
            else
            {
                if (HttpContext.Application["LoginStatus"].ToString() == "1")
                    return View(db.Animal.ToList());
                else
                    return RedirectToAction("Login", "LoginRegister");
            }
        }
        public ActionResult GulenIndex()
        {
            if (HttpContext.Application["LoginStatus"] == null)
            {
                return RedirectToAction("Login", "LoginRegister");
            }
            else
            {
                if (HttpContext.Application["LoginStatus"].ToString() == "1")
                    return View(db.Animal.ToList());
                else
                    return RedirectToAction("Login", "LoginRegister");
            }
        }
    }
}
