﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Patilendir.Startup))]
namespace Patilendir
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
